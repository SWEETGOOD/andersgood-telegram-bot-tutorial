# Revolutioniere deinen Kundenservice mit einem eigenen Telegram-Bot

In diesem Repository befinden sich alle Dateien die du benötigst, um deinen eigenen Telegram-Bot zum Leben zu erwecken.

[Hier gehts zum Tutorial.](https://andersgood.de/blog/revolutioniere-deinen-kundenservice-mit-einem-eigenen-telegram-bot-teil-1)

Das Tutorial baut auf der PHP-Bibliothek ["TelegramBot API"](https://github.com/TelegramBot/Api) auf.  
Weitere  Beispiele für Programmierbefehle findest du im [Wiki](https://github.com/TelegramBot/Api/wiki/Some-basic-examples).

---

Solltest du Unterstützung bei Umsetzung und Weiterentwicklung benötigen, stehe ich dir gerne zur Verfügung.  
**Hol dir gerne über den [https://t.me/botgoodDE_bot](@botgoodDE_bot) ein unverbindliches Angebot.**

---

Wenn du Feedback oder Anregungen hast, kannst du mich gerne über Telegram unter [@SWEETGOOD](https://t.me/SWEETGOOD) oder über meinen Blog unter [andersgood.de/kontakt](andersgood.de/kontakt) erreichen.