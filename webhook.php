<?php

#                                                                        #
#   _______          ________ ______ _______ _____  ____   ____  _____   #
#  / ____\ \        / /  ____|  ____|__   __/ ____|/ __ \ / __ \|  __ \  #
# | (___  \ \  /\  / /| |__  | |__     | | | |   _| |  | | |  | | |  | | #
#  \___ \  \ \/  \/ / |  __| |  __|    | | | |  | | |  | | |  | | |  | | #
#  ____) |  \  /\  /  | |____| |____   | | | |__| | |__| | |__| | |__| | #
# |_____/    \/  \/   |______|______|  |_|  \_____|\____/ \____/|_____/  #
#                                                                        #
#                              IT-Beratung mit Fokus auf Datensicherheit #
#                                                                        #
#         www.sweetgood.de | www.andersgood.de | www.botgood.de          #
#                                                                        #
# Copyright        : All rights reserved!
# Repository url   : https://codeberg.org/SWEETGOOD/andersgood-telegram-bot-tutorial
# Author           : Christian Süßenguth (codiflow)
# Filename         : webhook.php
# Created at       : 19.07.2020
# Last changed at  : 19.07.2020
# Input data       : POST-Request from Telegram servers
# Output data      : Custom Telegram bot logic
# Description      : Bot example script for the andersgood.de Tutorial
#                    which you can find at andersgood.de/blog
#
# You can get the icon codes for html_entity_decode from here:
# https://www.w3schools.com/charsets/ref_emoji.asp
#
# THANKS!
# I wanna thank the contributors of the great PHP TelegramBot API Library
# from https://github.com/TelegramBot/Api and the whole Telegram team for
# this cool and flexible bot API. Keep up your good work!

#########################
#     CONFIGURATION     #
#########################

# Set your bot API token here which you got from your chat with BotFather
# THIS IS NECESSARY FOR THE SCRIPT TO WORK!
$config_bot_api_token = "";

# You can define your telegram user id here if you want.
# Just start the bot t.me/useridgetbot and send him /get_me to get your user id
$config_admin_id = "";

#########################
# BASIC SECURITY CHECKS #
#########################

# Add some basic security check
# Must be a POST request
if ($_SERVER["REQUEST_METHOD"] != "POST")
{
	die("Wrong request!");
}

# Add some more security
# Requests need to come from an ip inside of the following ip ranges from the Telegram network:
# - 149.154.160.0/20
# - 91.108.4.0/22
$request_from = explode(".", $_SERVER["REMOTE_ADDR"]);

# Probably from 149.154.160.0/20
if (!($request_from[0] == 194 && $request_from[1] == 154 && $request_from[2] >= 160 && $request_from[2] <= 175 && $request_from[3] >= 1 && $request_from[3] <= 254))
{
	# Probably from 91.108.4.0/22
	if (!($request_from[0] == 91 && $request_from[1] == 108 && $request_from[2] >= 4 && $request_from[2] <= 7 && $request_from[3] >= 1 && $request_from[3] <= 254))
	{
		die("You may not request this!");
	}
}


#########################
#      SCRIPT START     #
#########################

require_once "vendor/autoload.php";

try
{
	$bot = new \TelegramBot\Api\Client($config_bot_api_token);

	/**
	 * For debugging only
	 *
	$bot->sendMessage(
		$config_admin_id,
		"Bot example script was executed!"
	);*/


#########################
#      START / STOP     #
#########################

	/**
	 * /start
	 * If someone starts the bot
	 */
	$bot->command('start', function ($message) use ($bot) {
		
		$bot->sendMessage(
			$message->getChat()->getId(),
			"Hallo <b>" . $message->getFrom()->getFirstName() . "</b>! " . html_entity_decode("&#127881;") . "\n\nUnd schon gehts los! Wirf einen Blick in das <code>[/]</code>-Menü, um zu sehen, was ich alles kann.",
			"HTML"
		);

	});

	/**
	 * /stop
	 * If someone stops the bot
	 */
	$bot->command('stop', function ($message) use ($bot) {

		$bot->sendMessage(
			$message->getChat()->getId(),
			"Schade, dass du gehst <b>" . $message->getFrom()->getFirstName() . "</b>! " . html_entity_decode("&#128148;"),
			"HTML"
		);

	});


#########################
#        COMMANDS       #
#########################

	/**
	 * /echo
	 * Sends the message back to the user he sent beforehand stripping the "/echo "
	 */
	$bot->command('echo', function ($message) use ($bot) {

		# Remove the first 5 characters (which contain /echo)
        $message_without_echo = substr($message->getText(), 5);

		# If theres no text after /echo
        if (strlen($message_without_echo) == 0)
		{
			$bot->sendMessage(
				$message->getChat()->getId(),
				"Bitte sende mir etwas Text nach deinem /echo-Befehl. Zum Beispiel: \"<code>/echo Testnachricht</code>\"",
				"HTML"
			);
		}
        # Otherwise if there's some text
		else
		{
			$bot->sendMessage(
				$message->getChat()->getId(),
				"Echo:" . $message_without_echo,
				"HTML"
			);
		}

	});

	/**
	 * /katze
	 * Sends you some cat content :-)
	 */
	$bot->command('katze', function ($message) use ($bot) {

		$bot->sendPhoto(
			$message->getChat()->getId(),
			"https://cataas.com/cat",
			"Was für ein schönes Katzenbild.",
			null,
			null,
			null,
			"HTML"
		);

	});


	# Runs the webhook for the bot
	$bot->run();

}
catch (\TelegramBot\Api\Exception $e)
{
	$e->getMessage();
}

?>
